// 1. Write a function that adds two numbers without using any arithmetic operators.

/*
We can use the xor, and to make this works. The table of the xor it is identical to sum table.
They only return 1 if the inputs are different. 
But when you sum sometimes there is a carry so the carry table it is also identical to the AND table.

eg.
a = 3, b = 4
in binary
a = 0011
b = 0100 

so at the first round, we make a XOR operation (if the inputs are differents the output is 1).

0011 ^ 0100 = 0111 = 7(decimal);

and then we check is there is any carry with AND (if the inputs are 1 the output it's 1 otherwise is 0)
0011 & 0100 = 0000 = 0(decimal) 
and then we use left shit to add one 0 to the end of the bit.
0000 << 1; shift to the left on bit the result is 00000 = 0 decimal

using recursivity we send back the new numbers (newA, newB)
make a validation if b is equal to 0 that means that there is no carry and return a in this case 7;


*/

const add = (a, b) => {
  if (b == 0) {
    return a;
  } else {
    const newA = a ^ b;
    const newB = (a & b) << 1;
    return add(newA, newB);
  }
};

console.log("1.-", add(3, 4));

// 2. Given 2 strings of unknown characters (but it cannot be repeated) create a function that returns an array of the characters that are repeated in both strings in
// the most efficient way.

/**
 * Set is a collection where that only contains unique values.
 * first pass the str1 to a set and I do the same for str2
 * then use filter to look into the set1 by character it is already on the set2 and return a new arrya
 */
const getCharactersRepeated = (str1, str2) => {
  const set1 = new Set([...str1]);
  const set2 = new Set([...str2]);
  const repeated = [...set1].filter((character) => set2.has(character));
  return repeated;
};
console.log("2.-", getCharactersRepeated("freddy", "danield"));

// 3.- Write a function that takes a string containing a number in base X along with an integer of the base X. The function must return the integer value of that
// string/base pair. E.g.,

/**
 * I'm not sure if a get right this exercise but in javascript there is already a function to
 * convert one string to a number in any base. parseInt(string, base);
 */
const convertToNumber = (number, base) => {
  const newNumber = parseInt(number, base);
  if (isNaN(newNumber)) {
    return "You should pass a valid string";
  }
  return newNumber;
};

console.log("3.- ", convertToNumber("F", 16));

// 4. Write a function such that if an element in an MxN matrix is 0 , its entire row and column are set to 0 and then printed out.

// 5. Write a function that convert the given number into a Roman Numeral - The function needs to receive a Number and Return a String (The Number can be
// between 1 and 3999)

/**
 * I created a constant where I placed all the values for the roman numbers each array contains the values.
 * eg. 345
 * 345 is divide by 1000 I get the remainder and the divide by 100 the result is 3 so goes to array hundres and get the value
 * of the array in the position 3 in this case XXX.
 * Then to get the tens I get the remainder divide by 100  and the divide by 10  the result is 4 so goes to the array and the
 * value in the position 4 (XL)
 * and finally the unist I do the a similiar operation.
 * Concatenate all these variables to get the roman number.
 */

const getRomanNumber = (number) => {
  const isNumber = (value) => typeof value === "number";

  if (!isNumber(number)) return "Pass a valid number";
  const ROMAN = {
    units: ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
    tens: ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
    hundreds: ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],
    thousandUnit: ["", "M", "MM", "MMM"],
  };

  // divide by 1000 to get the quantity of thousands they are in the number
  const thousandUnit = isNumber(number / 1000)
    ? ROMAN.thousandUnit[parseInt(number / 1000)]
    : "";

  // first I get the remainder of division between 1000 and then divide by 100 to get the quantity of hundreds.
  const hundreds = isNumber((number % 1000) / 100)
    ? ROMAN.hundreds[parseInt((number % 1000) / 100)]
    : "";

  // get tens
  const tens = isNumber((number % 100) / 10)
    ? ROMAN.tens[parseInt((number % 100) / 10)]
    : "";

  // get units
  const units = isNumber(number % 10) ? ROMAN.units[parseInt(number % 10)] : "";

  // concatenate the result of each variable
  const romanNumber = `${thousandUnit}${hundreds}${tens}${units}`;

  return romanNumber;
};

console.log("5.-", getRomanNumber(3451));

// 6. Write a function to print all permutations of a string. Max string length can be 50 characters.

// 7. Write a function that receives a sentence, and return the longest word, if two or more words has the same lenght, they are returned as an array, but can't return
// duplicated words.

/**
 * First I split the sentence by spaces then I iterate the array comparing each word and having one auxiliar variable to save the 
 * longest word.
 * I create a set to avoid duplicate words and then I make a filter to find another words with the same length.
 */
const getLongestWord = (sentence) => {
  const array = sentence.split(" ");
  let longestWord = "";
  for (let i = 0; i < array.length; i++) {
    if (array[i].length > longestWord.length) { // comparison to see wich word is the longest
      longestWord = array[i];
    }
  }
  const notDuplicated = new Set(array); // set to avoid duplicate words
  const wordsSameLenght = [...notDuplicated].filter( // find other words with the same length
    (word) => word.length === longestWord.length
  );
  if (wordsSameLenght.length > 1) {
    return wordsSameLenght;
  }
  return longestWord;
};

const s = getLongestWord("this is a test for longest word lenghts");
console.log("7.-", s);